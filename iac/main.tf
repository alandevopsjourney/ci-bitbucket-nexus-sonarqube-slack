#MODULE VCN
module "vcn" {
  source  = "oracle-terraform-modules/vcn/oci"
  version = "2.2.0"
  # insert the 7 required variables here
  compartment_id = oci_identity_compartment.tf-compartment.id
  region         = var.region-identifier
  vcn_name       = "${var.environment}-vnc"
  vcn_dns_label  = "${var.environment}subdev"
  # Optional
  vcn_cidr = var.vcn_cidr
  internet_gateway_enabled = var.internet_gateway_enabled
  nat_gateway_enabled = var.nat_gateway_enabled
  service_gateway_enabled = var.service_gateway_enabled
}

#PRIVATE SECURITY LIST
resource "oci_core_security_list" "private-security-list"{

# Required
  compartment_id = oci_identity_compartment.tf-compartment.id
  vcn_id = module.vcn.vcn_id

# Optional
  display_name = "security-list-for-private-subnet"

  egress_security_rules {
      stateless = false
      destination = "0.0.0.0/0"
      destination_type = "CIDR_BLOCK"
      protocol = "all" 
  }

  ingress_security_rules { 
      stateless = false
      source = "10.0.0.0/16"
      source_type = "CIDR_BLOCK"
      # Get protocol numbers from https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml TCP is 6
      protocol = "6"
      tcp_options  { 
          min = 22
          max = 22
      }
    }
}

#PUBLIC SECURITY LIST
resource "oci_core_security_list" "public-security-list"{

# Required
  compartment_id = oci_identity_compartment.tf-compartment.id
  vcn_id = module.vcn.vcn_id

# Optional
  display_name = "security-list-for-public-subnet"

    egress_security_rules {
      stateless = false
      destination = "0.0.0.0/0"
      destination_type = "CIDR_BLOCK"
      protocol = "all" 
  }

     ingress_security_rules {
       stateless = false
       source = "0.0.0.0/0"
       source_type = "CIDR_BLOCK"
       protocol = "all"
     }
     
 /*  ingress_security_rules { 
      stateless = false
      source = "0.0.0.0/0"
      source_type = "CIDR_BLOCK"
      # Get protocol numbers from https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml TCP is 6
      protocol = "6"
      tcp_options { 
          min = 22
          max = 22
      }
    }
  ingress_security_rules { 
      stateless = false
      source = "0.0.0.0/0"
      source_type = "CIDR_BLOCK"
      # Get protocol numbers from https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml ICMP is 1  
      protocol = "1"
  
      # For ICMP type and code see: https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml
      icmp_options {
        type = 3
        code = 4
      } 
    }   
  
  ingress_security_rules { 
      stateless = false
      source = "10.0.0.0/16"
      source_type = "CIDR_BLOCK"
      # Get protocol numbers from https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml ICMP is 1  
      protocol = "1"
  
      # For ICMP type and code see: https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml
      icmp_options {
        type = 3
      } 
    } */
}

#VCN PRIVATE SUBNET
resource "oci_core_subnet" "vcn-private-subnet"{

  # Required
  compartment_id = oci_identity_compartment.tf-compartment.id
  vcn_id = module.vcn.vcn_id
  cidr_block = var.cidr_block_private_subnet
 
  # Optional
  # Caution: For the route table id, use module.vcn.nat_route_id.
  # Do not use module.vcn.nat_gateway_id, because it is the OCID for the gateway and not the route table.
  route_table_id = module.vcn.nat_route_id
  security_list_ids = [oci_core_security_list.private-security-list.id]
  display_name = "private-subnet"
}

resource "oci_core_subnet" "vcn-public-subnet"{

  # Required
  compartment_id    = oci_identity_compartment.tf-compartment.id
  vcn_id            = module.vcn.vcn_id
  cidr_block        = var.cidr_block_public_subnet
 
  # Optional
  route_table_id = module.vcn.ig_route_id
  security_list_ids = [oci_core_security_list.public-security-list.id]
  display_name = "public-subnet"
}

resource "oci_core_dhcp_options" "dhcp-options"{

  # Required
  compartment_id    = oci_identity_compartment.tf-compartment.id
  vcn_id            = module.vcn.vcn_id
  options {
      type = "DomainNameServer"  
      server_type = "VcnLocalPlusInternet"
  }
  
  # Optional
  display_name      = "default-dhcp-options"
}



