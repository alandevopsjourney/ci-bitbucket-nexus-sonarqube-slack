variable "environment" {}
variable "tenancy-ocid" {}
variable "user-ocid" {}
variable "rsa-private-key-path" {}
variable "fingerprint" {}
variable "region-identifier" {}

#NETWORKING VARIABLES
variable "vcn_cidr" {}
variable "internet_gateway_enabled" {}
variable "nat_gateway_enabled" {}
variable "service_gateway_enabled" {}

variable "cidr_block_private_subnet" {}
variable "cidr_block_public_subnet" {}

#INSTANCE VARIABLES
variable "image-source-id" {}