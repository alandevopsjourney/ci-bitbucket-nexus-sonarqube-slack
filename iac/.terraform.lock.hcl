# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.36.0"
  constraints = "~> 3.35"
  hashes = [
    "h1:MA7T5ntGMjC54SGQg1YY6qeg4ouDTVdE8+fTuXbkyUQ=",
    "zh:17b1e924f1efc0084823bbcdd9565b6b7018b17e8471f65fc59c2d9617e28b60",
    "zh:1fd5dfe2d22db93c576c2f328b4a9cf28e09b88496888a54a25325c638a69bec",
    "zh:269d7e870fc86db4336ac215282ac0a97522209453ce6cc9022ab2079e4e7a3b",
    "zh:3b107da332bccec4a6e60ed790323d7cc8d264d8b5709cb14a931728bb06241e",
    "zh:74480ff5c05f9156f32c9d93c2c43bb13d7ae21bdc85b727550fe4c1812252ca",
    "zh:79c60989f44dba2851c790dc15f18b80ba811e6140260cc42b9fe343656e3a01",
    "zh:7f6fd96abd233acc52ec412bc72fb52784e47475f16eaa908e72c4c4b0997109",
    "zh:b2fba2820c505a10211199112d3e16cac224b638ba6f8b09b8b635746cc49a11",
    "zh:c4f53cb16f5e7439cefcddcc2e91798e5ec6a11ca6f9442e2ec509cd0859625c",
    "zh:ec54a1b2ffbec157fad0b6e0efc0d8da1e1153874060d47497ab1a5e9d6ab26f",
    "zh:f01d0fe3f7757fe290dc7889ffe1d926da665a7a0bb895b9f2518c3ac5c6963c",
  ]
}

provider "registry.terraform.io/hashicorp/oci" {
  version     = "4.28.0"
  constraints = ">= 4.0.0"
  hashes = [
    "h1:ggnRKclGVCa02as9nZaLMZ91Zpeddo4n3e0b3VZ5zGA=",
    "zh:432585493768502dd11d42922fd41d9908755a279910dc52d365cd47353edff5",
    "zh:442e11ef4af81946738010a609d022d1b7c1f1620d64221899fec9cae17d3c32",
    "zh:b768157d0ee8c19fe07ab661dc938af4849024abfc10e104ac9951715dceaad8",
    "zh:c895d32dd69d42532de71a9912725e6bc9be2fe3a6000cbce0531029778fe33f",
    "zh:daea41b433b11dc752e3123d35b287429c734774dcb03323b839bec15f96d52b",
    "zh:e1b74ff29cca43202aef4708276db16812c10cf16e9080cd0e48b7250390324a",
    "zh:ebd82e5f4d956ad82bafc7eba32e298725b815ded312c1164e7317eab7445147",
    "zh:ecf83a0d8065e257e28a49509455f86942446cb4633d7c63e358926962a60d6a",
    "zh:ed419402cc6dd7908cc9cab980d503a02004ecd09b6f7de206c30300a9d34a19",
    "zh:edcd7fbb9c7a5954368a7c975bb7d1b9f77c8e452bdab4d586470691b9d4133a",
  ]
}
