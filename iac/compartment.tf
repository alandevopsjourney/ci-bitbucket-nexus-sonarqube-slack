#CREATE A COMPARTMENT
resource "oci_identity_compartment" "tf-compartment" {
  # Required
  compartment_id = var.tenancy-ocid
  description    = "Compartment for Terraform resources."
  name           = "Terraform_Study"
}
