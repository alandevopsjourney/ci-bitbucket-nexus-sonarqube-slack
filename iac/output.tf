# The "name" of the availability domain to be used for the compute instance.
output "name-of-first-availability-domain" {
  value = data.oci_identity_availability_domains.ads.availability_domains[0].name
}

# The "Name" of the compartment
output "compartment-name" {
  value = oci_identity_compartment.tf-compartment.name
}
# The "ID" of the compartment
output "compartment-OCID" {
  value = oci_identity_compartment.tf-compartment.id
}

# Outputs for VCN MODULE
output "vcn_id" {
  description = "OCID of the VCN that is created"
  value       = module.vcn.vcn_id
}


# Outputs for PRIVATE SECURITY LIST
output "private-security-list-name" {
  value = oci_core_security_list.private-security-list.display_name
}
output "private-security-list-OCID" {
  value = oci_core_security_list.private-security-list.id
}

# Outputs for PUBLIC SECURITY LIST
output "public-security-list-name" {
  value = oci_core_security_list.public-security-list.display_name
}
output "public-security-list-OCID" {
  value = oci_core_security_list.public-security-list.id
}

# Outputs for PRIVATE SUBNET
output "private-subnet-name" {
  value = oci_core_subnet.vcn-private-subnet.display_name
}
output "private-subnet-OCID" {
  value = oci_core_subnet.vcn-private-subnet.id
}

# Outputs for PUBLIC SUBNET
output "public-subnet-name" {
  value = oci_core_subnet.vcn-public-subnet.display_name
}
output "public-subnet-OCID" {
  value = oci_core_subnet.vcn-public-subnet.id
}

# Outputs for DHCP Options
output "dhcp-options-name" {
  value = oci_core_dhcp_options.dhcp-options.display_name
}
output "dhcp-options-OCID" {
  value = oci_core_dhcp_options.dhcp-options.id
}

# Output for PUBLIC COMPUTE
output "compute_public_ip_nexus" {
  description = "Show the compute public ip"
  value = oci_core_instance.nexus_instance.public_ip
}